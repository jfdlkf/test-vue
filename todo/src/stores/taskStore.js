import { defineStore } from 'pinia';

export const useTaskStore = defineStore('taskStore', {
  state: () => ({
    tasks: [],
  }),
  actions: {
    addTask(task) {
      this.tasks.push(task);
    },
    deleteTask(taskId) {
      this.tasks = this.tasks.filter(task => task.id !== taskId);
    },
    markTaskAsCompleted(taskId) {
      const task = this.tasks.find(task => task.id === taskId);
      if (task) task.completed = true;
    },
  },
});
