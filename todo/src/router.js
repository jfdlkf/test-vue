import { createRouter, createWebHistory } from 'vue-router';
import TaskList from './components/taskList.vue';
import TaskArchive from './components/taskArchive.vue';

const routes = [
  { path: '/', component: TaskList },
  { path: '/archive', component: TaskArchive },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
