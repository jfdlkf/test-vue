import { createI18n } from 'vue-i18n';

const messages = {
  en: {
    welcome: 'Welcome',
    taskList: 'Task List',
    taskArchive: 'Task Archive',
  },
  fr: {
    welcome: 'Bienvenue',
    taskList: 'Liste des tâches',
    taskArchive: 'Archive des tâches',
  },
};

const i18n = createI18n({
  locale: 'en',
  messages,
});

export default i18n;
